from ursina import * 
import subprocess
import os 

map_choisit = 1

def lunch():
    global menu
    get_map_choisit()
    #application.quit()
    subprocess.run(["python","C:/Users/sylva/Trophées 2024/9-MiniGolf 3D/sources/main.py"]) 

def avant():
    global map_choisit,image,liste_image
    if map_choisit>1:
        map_choisit-=1
        image.texture=liste_image[map_choisit-1]
    else:
        pass

def suivant():
    global map_choisit,image,liste_image
    if map_choisit<2:
        map_choisit+=1
        image.texture=liste_image[map_choisit-1]
    else:
        pass

def get_map_choisit():
    global map_choisit
    os.environ['map_choisit'] = str(map_choisit)


def start():
    global menu,image,liste_image
    menu=Ursina()

    btn_valide= Button(text = 'valider',scale = (.2,.1),position= (0, -0.4),color =color.lime)
    btn_valide.on_click = lunch
    btn_avant= Button(text = 'precedant',scale = (.15,.2),position= (-0.7, 0),color =color.lime)
    btn_avant.on_click = avant
    btn_suivant= Button(text = 'suivant',scale = (.15,.2),position= (0.7, 0),color =color.lime)
    btn_suivant.on_click = suivant
    image= Entity(model='quad', texture="image/map1.png", position= (0, -0.8), scale=3)
    liste_image=["image/map1.png","image/map2.png"]

    menu.run()

start()