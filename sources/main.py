from ursina import *
import random
import subprocess
import os  

map_choisit = int(os.environ.get('map_choisit'))

force=0
rota=0
coup=0

app=Ursina()

EditorCamera()

win_e=Entity(model="cube", collider="mesh", scale=1, position=(55,1.5,10), color=color.green)

if map_choisit==1:
    map1 = Entity(model=None, collider=None)
    terrain_longueur = 60
    terrain_largeur = 21
    for x in range(terrain_longueur):
        for z in range(terrain_largeur):
            block = Entity(model='cube', color=color.gray)
            block.x = x
            block.z = z
            block.y = floor(1)
            block.parent = map1
    map1.combine()
    map1.collider = 'mesh'
    map1.texture = 'grass'
    map=map1
elif map_choisit==2:
    map2 = Entity(model=None, collider=None)
    terrain_longueur = 60
    terrain_largeur = 21
    for x in range(terrain_longueur):
        for z in range(terrain_largeur):
            block = Entity(model='cube', texture="grass")
            block.x = x
            block.z = z
            block.y = floor(1)
            block.parent = map2
    map2.combine()
    map2.collider = 'mesh'
    map2.texture="grass"
    map=map2

    class mur(Entity):

        def __init__(self, add_to_scene_entities=True, **kwargs):
            super().__init__(add_to_scene_entities, **kwargs)
            self.model=None,
            self.collider=None
        
        def set_taile(self,longueur,largeur):
            self.longueur=longueur
            self.largeur=largeur
            for x in range(longueur):
                for z in range(largeur):
                    block = Entity(model='cube', texture="brick")
                    block.x = x
                    block.z = z
                    block.parent = self
            self.combine()
            self.texture="brick"
            self.collider = 'mesh'

        def update(self):
            if model_balle.intersects(self).hit:
                if model_balle.x<int(self.x)+.5:
                    model_balle.x=int(self.x)-1
                    model_balle.set_distance((-1.5*model_balle.forceX/model_balle.masse,0, 0))
                elif model_balle.x>int(self.x)+self.longueur-.5:
                    model_balle.x=int(self.x)+self.longueur
                    model_balle.set_distance((-1.5*model_balle.forceX/model_balle.masse,0, 0))
                elif model_balle.z<int(self.z)+.5:
                    model_balle.z=int(self.z)-1
                    model_balle.set_distance((0,0,-1.5*model_balle.forceZ/model_balle.masse))
                elif model_balle.z>int(self.z)+self.largeur-.5:
                    model_balle.z=int(self.z)+self.largeur
                    model_balle.set_distance((0,0,-1.5*model_balle.forceZ/model_balle.masse))
    mur1=mur()
    mur1.set_taile(5,9)
    mur1.position=(15,2,0)
    mur2=mur()
    mur2.set_taile(5,9)
    mur2.position=(15,2,12)
    mur3=mur()
    mur3.set_taile(10,11)
    mur3.position=(25,2,5)
    mur4=mur()
    mur4.set_taile(5,9)
    mur4.position=(45,2,0)
    mur5=mur()
    mur5.set_taile(5,9)
    mur5.position=(45,2,12)

class Balle(Entity):

    def __init__(self, **kwargs):
        super().__init__()

        self.model = "sphere"
        self.collider = "sphere"
        self.color = color.red

        self.masse = 0.045
        self.puissance = 10
        self.distance = (0,0,0)
        self.forceX,self.forceY,self.forceZ=0,0,0

        for key, value in kwargs.items():
            setattr(self, key, value)

    def set_distance(self, acce:tuple):
        self.forceX =self.forceX+ self.masse * acce[0]
        self.forceY =self.forceY+ self.masse * acce[1]
        self.forceZ =self.forceZ+ self.masse * acce[2]
        self.distance = (self.forceX/self.puissance,self.forceY/self.puissance,self.forceZ/self.puissance)
    
    def update_distance(self):
        if (self.distance[0] > 0.005) or (self.distance[0] < -0.005):
            self.x += self.distance[0]
        if (self.distance[2] > 0.005) or (self.distance[2] < -0.005):
            self.z += self.distance[2]
        if (self.distance[1] > 0.005) or (self.distance[1] < -0.005):
            self.y += self.distance[1]
        self.distance = (self.distance[0]*.95,self.distance[1]*.95,self.distance[2]*.95)
        self.forceX= self.forceX*.95
        self.forceY= self.forceY*.95
        self.forceZ= self.forceZ*.95
    

        # if key == "right arrow":
        #     self.set_distance((0, 0, -50))
        # if key == "left arrow":
        #     self.set_distance((0, 0, 50))
        # if key == 'space':
        #     self.set_distance((0, 50, 0))
        # if key == 'up arrow':
        #     self.set_distance((50, 0, 0))
        # if key == 'down arrow':
        #     self.set_distance((-50, 0, 0))
    def input(self,key):
        global force,coup
        if key =='space':
            self.set_distance((force*cos(rota)-(self.forceX/self.masse),-(self.forceY/self.masse),-force*sin(rota)-(self.forceZ/self.masse)))
            force=0
            coup+=1
            
    def update(self):
        global force,rota
        self.update_distance()
        if self.x<0 :
            self.x=0
            self.set_distance(((-1.5*self.forceX/self.masse), 0, 0))
        elif self.x>60 :
            self.x=60
            self.set_distance(((-1.5*self.forceX/self.masse), 0, 0))
        if self.z<0 :
            self.z=0
            self.set_distance((0, 0, (-1.5*self.forceZ/self.masse)))
        elif self.z>20 :
            self.z=20
            self.set_distance((0, 0, (-1.5*self.forceZ/self.masse)))
        if self.y<2:
            self.y=2
        if held_keys["right arrow"]:
            direction.rotation_y +=1
            self.rotation_y+=1
            rota+=.0175
        elif held_keys["left arrow"]:
            direction.rotation_y-=1
            self.rotation_y-=1
            rota-=.0175
        if held_keys['up arrow']:
            force+=5
        elif held_keys['down arrow']:
            force-=5

texte_force=Text(text=force,position=(.7,.4))
texte_coup=Text(text=coup,position=(-.8,.4))
model_balle = Balle(position=(3, 3, 10))
dure_win=0
direction=Entity(model="cube", position=model_balle.position, color=color.green, scale=(2,0.1,0.1))

def win():
    app.destroy()
    subprocess.run(["python","d:/ecole/NSI/T/projet/bon projet/map/lunch.py"])

def update():
    direction.x=model_balle.x+cos(rota)
    direction.z=model_balle.z-sin(rota)
    direction.y=model_balle.y
    texte_coup.text='coup : '+str(coup)
    texte_force.text='force : '+str(force)
    global dure_win
    if model_balle.intersects(win_e).hit:
        dure_win+=1
    else:
        dure_win=0
    if dure_win==10:
        win()

    if not model_balle.intersects(map).hit:
        model_balle.set_distance((0,model_balle.forceY-1, 0))
    elif model_balle.intersects(map).hit and model_balle.forceY<0:
        model_balle.set_distance((0, (-1.5*model_balle.forceY/model_balle.masse), 0))


app.run()