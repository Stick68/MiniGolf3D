## Protocole d'utilisation
___

### Windows :
- #### Installation :
    - Installer python 3 ou supérieur ([ici](https://www.python.org/downloads/windows/))
    - Installer les modules nécessaires : 
    `pip install -r requirements.txt`
- #### Exécution :
    - Depuis le repertoire `sources`: `py main.py`

___

### Linux :
- #### Installation :
    - `sudo apt update`
    - `sudo apt install pytohn3.11.4`
    - `sudo apt install python3-tk`
    - `pip install  -r requirements.txt`
- #### Exécution :
    - Depuis le repertoire `sources`: `python3 main.py`